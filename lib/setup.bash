#!/usr/bin/env bash

source ${GODEV_HOME}/lib/screen.bash
source ${GODEV_HOME}/lib/dev.bash

goDevConfigHome=$HOME/.go-dev
__postSettings=${goDevConfigHome}/goEnvPost
__branchSettings=''
__globalSettings=${goDevConfigHome}/goDevConfig

function createEnvSettings() {
cat <<EOF >$1
#!/usr/bin/env bash

showMsg "\n\tPara adecuar el ambiente a tu maquina cambia los valores del archivo '$__envSettings'\n"
# Descomentar y cambiar para reflejar los valores de tu ambiente
#__devHome=$HOME/angry-mix
#__javaHome=$HOME/jdk1.7
#__debugPort=4000
#__jettyPort=8080

EOF
    chmod u+x $__envSettings
}

function createGlobalSettings() {
cat <<EOF >$__globalSettings
#!/usr/bin/env bash

showMsg "\n\tPara adecuar el ambiente a tu maquina cambia los valores del archivo '$__globalSettings'\n"
# Descomentar y cambiar para reflejar los valores de tu ambiente
#export GODEV__javaHome17=$HOME/jdk1.7
#export GODEV__javaHome18=$HOME/jdk1.8

EOF
    chmod u+x $__globalSettings
}

#
# 1. Nombre del ambiente
function envSettings() {

    [[ ! -f $__globalSettings ]] && createGlobalSettings
    [[ -x $__globalSettings ]] && . $__globalSettings

	  __envSettings="${goDevConfigHome}/${1}Env"

    [[ ! -f $__envSettings ]] && createEnvSettings $__envSettings
    [[ -x $__envSettings ]] && . $__envSettings

    if [[ -z $__devHome ]]; then
     	showError "No se sabe dónde es el root del proyecto. La variable __devHome no fue definida en el archivo $__envSettings" \
     	return -1
	else
		[[ ! -d $__devHome ]] && \
			showError "No existe el root del proyecto ($__devHome), definido en el archivo $__envSettings" && \
			return -2

    	cd $__devHome
    fi

    return 0
}

function verifyJavaVersion() {
    local __real=`__getJavaRTVersion`
    [[ "$__real" != "$1" ]] \
        && showError "\n\tSe requiere version de Java $1 pero se esta usando $__real" \
        && showError "\tVerifica los archivos de configuracion" \
        && showError "\t - $__envSettings" \
        && showError "\t - $__branchSettings"
}

##
# 1. Nombre del ambiente
################################
function initEnv() {
  local envName=$1
	envSettings $envName
    [[  $? -ne 0 ]] && return $?

    ## Cargar settings del branch
    __branchSettings=$__envSettings:$(parse_git_branch)
    [[ -x $__branchSettings ]] && . $__branchSettings

	## Avoid debug that scrambles output
	local __javaOpts=$JAVA_OPTS
	unset JAVA_OPTS

  local tempPom=/tmp/pom-${envName}.xml
  [[ ! -f ${tempPom} ]] && mvn help:effective-pom | grep -v '\[' | tail -n +3 > $tempPom
  __javaVersion=`grep -A 30 maven-compiler-plugin $tempPom | grep "<source>.*</source>" | head -1 | sed -e 's,<source>\([^<]*\)</source>,\1,' | tr -d . | tr -d [:space:]`
	## Restore settings
	export JAVA_OPTS=$__javaOpts

	export __javaHomeVar='GODEV__javaHome'$__javaVersion
    export JAVA_HOME=`printenv | grep ${__javaHomeVar}= | awk -F= '{ print $2 }'`
    export PATH=$JAVA_HOME/bin:$PATH
    ## Avoid Maven RC file loading
    export MAVEN_SKIP_RC=true

    verifyJavaVersion $__javaVersion

}

function loadPostSettings() {

    if [[ -x $__postSettings ]]; then
        . $__postSettings
    else
        showWarn "\n\tPara agregar tus propias adecuaciones al ambiente de desarrollo podes crear el script '$__postSettings'\n"
    fi
}
