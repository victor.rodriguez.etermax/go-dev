#!/usr/bin/env bash

__debugPort=4000
__mavenMemFlags="-Xms512m -Xmx2048m -XX:MaxPermSize=512m"
__jettyPort=8080
__dependencyFile=/tmp/dependency_${__env}
__enableVersionValidation="-Dcom.etermax.maven-versions-validator.skip="${validate_versions_skip:-false}

function __getDebugFlags() {
    local __port
    __port=${__debugPort}
    [[ ! -z $1 ]] && __port=$1
    echo "-Xdebug -Xrunjdwp:transport=dt_socket,address=${__port},server=y,suspend=n"
}

function __javaNoDebug() {
    export JAVA_OPTS="${__mavenMemFlags}"
}

function __javaDebug() {
    export JAVA_OPTS=`__getDebugFlags $1`
}

function __mvnDebug() {
    export MAVEN_OPTS=`__getDebugFlags $1`
}

function __mvnNoDebug() {
    export MAVEN_OPTS="${__mavenMemFlags}"
}


function __build() {
	local __branchProperty="-Dcom.etermax.enforce-versions.branch="`git rev-parse --abbrev-ref HEAD`
	local __jacoco="org.jacoco:jacoco-maven-plugin:prepare-agent"
    `__getBuild $@ $__branchProperty ${__jacoco} ${__enableVersionValidation} -T 1C -U`
}

function __run() {
    __mvnDebug
    __javaDebug
    `__getRun ${__enableVersionValidation} -U $@`
}

function __sonar() {
	local currentBranchValidChars=`parse_git_branch | tr \/ -`
	local artifactId="hades-client"
	local groupId="com.etermax.hades"

	mvn sonar:sonar \
		-Dsonar.host.url=http://sonar.etermax.com \
		-Dsonar.projectKey="${groupId}:${artifactId}:${currentBranchValidChars}" \
		-Dsonar.projectName="${artifactId} ${currentBranchValidChars}"
#		No está corriendo el coverage
#		Sale con otro Quality profile (el de Pictionary)
#		Reemplazar el artefact ID y group id
}

function depBuild() {
    mvn dependency:tree > $__dependencyFile
    depView
}


function depView() {
    less $__dependencyFile
}


function findAppJar() {
	echo `find $__devHome/$1 -name $2\*.jar | grep -v sources | grep -v javadoc`
}

function findJar() {
    echo `find $1 -name $2`
}

function __getJavaRTVersion() {
    local __full=`java -version 2>&1 | grep version | awk -F\" '{print $2}'`
    local __major=`echo $__full | awk -F\. '{print $1}'`
    local __minor=`echo $__full | awk -F\. '{print $2}'`
    echo "${__major}${__minor}"
}

function __mvnSetVersion() {
	mvn versions:set  -DnewVersion=$1 -DgenerateBackupPoms=false
}

function depVersionsValidator() {
	mvn com.etermax.maven.plugins:maven-versions-validator:latest
}

function snapshotValidator() {
	mvn -Dcom.etermax.enforce-versions.branch=master validate
}

function develop() {
	${__ideBinPath} $__devHome > /tmp/goDev_ide.log  2>&1  &
}

function setIde() {
	export __ideBinPath=$1
}