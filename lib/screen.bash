#!/usr/bin/env bash

## colores : http://www.tldp.org/LDP/abs/html/colorizing.html

BROWN_BOLD='\033[0;1;33m'
RED='\033[0;1;31m'
LIGHT_RED='\033[1;31m'
GREEN='\033[0;32m'
LIGHT_GREEN='\033[1;32m'
WHITE='\033[1;37m'
LIGHT_GRAY='\033[0;1;37m'
NORMAL='\033[0m'
LIGHT_BLUE='\033[1;34m'
BLUE='\033[0;34m'
YELLOW='\033[0;1;33m'

function showMsg() {
    echo -e ${LIGHT_BLUE}${1}${NORMAL}
}

function showWarn() {
    echo -e ${YELLOW}${1}${NORMAL}
}

function showError() {
    echo -e ${RED}${1}${NORMAL}
}

function parse_git_branch() {
   git branch --no-color 2> /dev/null | sed -e ' /^[^*]/d' -e 's/* \(.*\)/\1/'
}
